var WebSocketServer = require('ws').Server
const fetch = require('node-fetch');
const fs = require('fs');
const PORT = process.env.PORT || 3000;
var wss = new WebSocketServer({
    port: PORT
})
var wsList = []
var sayi = 0
var maclar = []
var live_api = '//rakipbahis01.com/socket_data/live'
var sender_seconds = 3000
var fetch_seconds = 3000


/*
var io = require('socket.io-client');
var socket = io.connect('http://live.redsoftnv.com', {reconnect: true});
//https://github.com/socketio/socket.io-client
var testDemo = 0
if (testDemo === 0) {

  socket.emit('type', 'betting');
  socket.on('live', (data) => {
    for( let ws of wsList){
      ws.send(JSON.stringify(data))
    }
    console.log(wsList.length)
  })
  testDemo = 1
}
*/




wss.on('connection', (ws) => {
    wsList.push(ws)

    sendData();

    ws.on('close', () => {
        wsList.splice(wsList.indexOf(ws), 1)
    })
})


sendWithMap = (data) => {
    wsList.map((ws) => {
        ws.send(JSON.stringify(data))
    })
}

getLiveMatchsData = () => {
    if (wsList.length) {
        fetch('https//rakipbahis01.com/socket_data/live').then((response) => {
            return response.json();
        }).then((data) => {
            fs.writeFile('betradar.json', JSON.stringify(data, null, 2), (err) => {
                if (err) throw err;
            });
        });
    }
}

sendData = () => {
    if (wsList.length) {
        for (let ws of wsList) {
            fs.readFile('betradar.json', 'utf8', (err, data) => {
              console.log(data);
                if (err) throw err;
                ws.send(data)
            });
        }
    }
}

setInterval(sendData, sender_seconds);
setInterval(getLiveMatchsData, fetch_seconds);
