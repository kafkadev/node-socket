var WebSocketServer = require('ws').Server
const fetch = require('node-fetch');
const _ = require('underscore');

const PORT = process.env.PORT || 3000;
var wss = new WebSocketServer({
    port: PORT
})



var live_api = "https://rakipbahis01.com/socket_data/live"
var listener_api = "https://rakipbahis01.com/socket_data/listeners"

var wsList = []
var matches = []
var listeningData = {
    matches: {},
    markets: {}
}
var listening_ids = []

/* SOCKET */

wss.on('connection', (ws) => {
    ws.listening_id = 0
    wsList.push(ws);
    sendLiveMatchesData();
    ws.on('message', (message) => {
        message = IsJsonString(message);

        if (message && message.type) {
            if (message.type == 1) {

            }
            if (message.type == 2) {
                if (message.data) {
                    ws.listening_id = message.data
                } else {
                    ws.listening_id = 0
                }
                // console.log('kullanıcı id değişti', message.data)
            }
        }

    });


    ws.on('close', () => {
        wsList.splice(wsList.indexOf(ws), 1);
        //console.log('cikis yapildi');
    });

});


/* SOCKET */



/* METHODS */

sendWithMap = (data) => {
    wsList.map((ws) => {
        ws.send(JSON.stringify(data))
    })
}



getLiveMatchsData = () => {
    if (wsList.length) {
        fetch(live_api).then((response) => {
            return response.json();
        }).then((data) => {
            matches = data
        });
    }
}


sendLiveMatchesData = () => {
    if (wsList.length) {
        for (let ws of wsList) {
            ws.send(setFormatData(1, matches));
        }
    }
}




/* LISTENERS */

/*
Kullanıcıların belli aralıklarla dinlediği maçların id'ini alır */
checkListeningIds = () => {
    if (wsList.length) {
        let merge = []
        for (let ws of wsList) {
            if (ws.listening_id) {
                merge.push(ws.listening_id)
            }

        }
        listening_ids = _.uniq(merge).filter(Boolean)
            //console.log(merge, listening_ids)
    }

}


/* kullanıcılar tarafından dinlenen datayı aralıklarla günceller */
getListeningData = () => {
    if (wsList.length && listening_ids.length) {

        fetch(listener_api, {
                method: 'POST',
                body: JSON.stringify({
                    listen_ids: listening_ids
                })
            })
            .then(res => res.json())
            .then((resjson) => {

                listeningData.matches = _.indexBy(resjson.matches, 'id')
                listeningData.markets = _.groupBy(resjson.markets, 'mac_id')

               // console.log(listening_ids)
            });
    }
}

/* dinlenen ve güncelllenen datayı belli aralıklarla kullanıcılara gönderir */
sendListeningData = () => {
    if (wsList.length && listening_ids.length) {
        for (let ws of wsList) {
            if (ws.listening_id && listeningData.matches && listeningData.matches[ws.listening_id]) {
                let SendData = {
                    match : listeningData.matches[ws.listening_id],
                    markets: listeningData.markets[ws.listening_id],
                    match_id: ws.listening_id
                }
                ws.send(setFormatData(2, SendData));
            }
        }
    }
}



/* LISTENERS */

IsJsonString = (str) => {
    try {
        str = JSON.parse(str);
    } catch (e) {
        return false;
    }
    return str;
}

setFormatData = (type, preData) => {
    /*
types = 1: listen ids
  */
    let format = {
        type: type,
        data: preData
    }
    return JSON.stringify(format);
}


/* METHODS */








/*
heartbeat = () => {
    this.isAlive = true;
}
setInterval(() => {
  wss.clients.forEach((socket) => {

    if ([socket.OPEN, socket.CLOSING].includes(socket.readyState)) {
      console.log(socket.OPEN, socket.CLOSING, socket.readyState);
     // socket.terminate();
    }
  });
}, 100);

function noop() {}




const interval = setInterval(function ping() {
  wss.clients.forEach(function each(ws) {
    if (ws.isAlive === false) return ws.terminate();


  });
}, 100);
*/




setInterval(sendLiveMatchesData, 3000);
setInterval(getLiveMatchsData, 3000);
/* Listenin işlemleri */
setInterval(checkListeningIds, 3000);
setInterval(getListeningData, 3000);
setInterval(sendListeningData, 3000);
