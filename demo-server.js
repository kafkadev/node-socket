var WebSocketServer = require('ws').Server
const fetch = require('node-fetch');
const _ = require('underscore');

const PORT = process.env.PORT || 3000;
var wss = new WebSocketServer({
    port: PORT
})



var live_api = "https://rakipbahis01.com/socket_data/live"
var listener_api = "https://rakipbahis01.com/socket_data/listeners"

var wsList = []
var matches = []
var listeningData = []
var listening_ids = []

/* SOCKET */

wss.on('connection', (ws) => {
    ws.listening_ids = []
    wsList.push(ws);

    ws.on('message', (message) => {
        message = IsJsonString(message);

        if (message && message.data && message.type) {
            if (message.type == 1) {
                ws.listening_ids = message.data
            }
            if (message.type == 2) {

            }
        }

    });


    ws.on('close', () => {
        wsList.splice(wsList.indexOf(ws), 1);
        console.log('cikis yapildi');
    });

});


/* SOCKET */



/* METHODS */

sendWithMap = (data) => {
    wsList.map((ws) => {
        ws.send(JSON.stringify(data))
    })
}



getLiveMatchsData = () => {
    if (wsList.length) {
        fetch(live_api).then((response) => {
            return response.json();
        }).then((data) => {
            matches = data
        });
    }
}


sendLiveMatchesData = () => {
    if (wsList.length) {
        for (let ws of wsList) {
            ws.send(setFormatData(1, matches));
        }
    }
}




/* LISTENERS */

/*
Kullanıcıların belli aralıklarla dinlediği maçların id'ini alır */
checkListeningIds = () => {
    if (wsList.length) {
        let merge = []
        for (let ws of wsList) {
            if (ws.listening_ids.length) {
                merge.push(ws.listening_ids)
            }
        }
        listening_ids = _.uniq(_.flatten(merge))
        console.log(listening_ids)
    }

}


/* kullanıcılar tarafından dinlenen datayı aralıklarla günceller */
getListeningData = () => {
    if (wsList.length && listening_ids.length) {

        fetch(listener_api, {
                method: 'POST',
                body: JSON.stringify({
                    listen_ids: listening_ids
                })
            })
            .then(res => res.json())
            .then((resjson) => {

                listeningData = resjson

                console.log(listening_ids, resjson)
            }
            );
    }
}

/* dinlenen ve güncelllenen datayı belli aralıklarla kullanıcılara gönderir */
sendListeningData = () => {
    if (wsList.length && listening_ids.length) {
        for (let ws of wsList) {
            if (ws.listening_ids.length) {
                ws.send(setFormatData(2, listeningData));
            }
        }
    }
}



/* LISTENERS */

IsJsonString = (str) => {
    try {
        str = JSON.parse(str);
    } catch (e) {
        return false;
    }
    return str;
}

setFormatData = (type, preData) => {
    /*
types = 1: listen ids
  */
    let format = {
        type: type,
        data: preData
    }
    return JSON.stringify(format);
}


/* METHODS */








/*
heartbeat = () => {
    this.isAlive = true;
}
setInterval(() => {
  wss.clients.forEach((socket) => {

    if ([socket.OPEN, socket.CLOSING].includes(socket.readyState)) {
      console.log(socket.OPEN, socket.CLOSING, socket.readyState);
     // socket.terminate();
    }
  });
}, 100);

function noop() {}




const interval = setInterval(function ping() {
  wss.clients.forEach(function each(ws) {
    if (ws.isAlive === false) return ws.terminate();


  });
}, 100);
*/




setInterval(sendLiveMatchesData, 10000);
setInterval(getLiveMatchsData, 10000);
/* Listenin işlemleri */
setInterval(checkListeningIds, 10000);
setInterval(getListeningData, 10000);
setInterval(sendListeningData, 10000);
